"use strict";

var express = require('express');
var path = require('path');
var _ = require('lodash');
var fp = require('lodash/fp');
var Promise = require('bluebird');
var uuid = require('uuid');
var app = express();

app.get('/ping', function(req, res){
    res.status(200).send('OK');
});

var publicDir = path.join(__dirname, 'public');
app.use(express.static(publicDir, {
    maxAge: '20s'
}));

var rooms = [
    'living room',
    'dining room',
    'bedroom',
    'garage',
    'hallway',
    'laundry',
];

var places = [
    'north',
    'east',
    'south',
    'west',
];

var itemtasks = [
    'sweep',
    'vacuum',
    'mop',
    'dry',
];

var assignees = [
    'John',
    'Paul',
    'Ringo',
    'George',
];

var parts = _.range(0,5);

var tasks = fp.flow(
    fp.map(function(roomA){
        return {room: roomA};
    }),
    fp.map(function(obj){
        return fp.map(function(place){
            return _.merge({}, obj, {place: place});
        }, places);
    }),
    fp.flatten,
    fp.map(function(obj){
        return fp.map(function(task){
            return _.merge({}, obj, {task: task});
        }, itemtasks);
    }),
    fp.flatten,
    fp.map(function(obj){
        return fp.map(function(i){
            return _.merge({}, obj, {part: i});
        }, parts);
    }),
    fp.flatten,
    fp.map(function(obj){
        obj.uuid = uuid.v4();
        obj.assignee = _.sample(assignees);
        return obj;
    }),
    fp.shuffle
)(rooms);

app.get('/tasks', function(req, res){
    Promise.resolve(tasks).delay(3000).then(function(){
        res.json(_.shuffle(tasks));
    });
});

app.listen(8080, 'localhost');

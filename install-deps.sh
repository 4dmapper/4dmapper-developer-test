#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

sudo apt-get -y update
sudo apt-get -y install curl

curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs

sudo npm install -g gulp

npm install

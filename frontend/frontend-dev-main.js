"use strict";

var TaskListViewModel = require('./TaskList/TaskListViewModel');
var TaskListService = require('./TaskList/TaskListService');
var TaskListModel = require('./TaskList/TaskListModel');

function Application() {
    this.taskListService = new TaskListService(this);
    this.taskListModel = new TaskListModel(this, this.taskListService);
    this.taskListViewModel = new TaskListViewModel(this, this.taskListModel);
}

Application.prototype.load = function(){
    this.taskListViewNodes = this.taskListViewModel.load();
}

var app = new Application();
app.load();

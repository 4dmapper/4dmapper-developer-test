"use strict";

var Promise = require('bluebird');

function DemoTextModel(app, demoTextService) {
    this.app = app;
    this.demoTextService = demoTextService;

    this.ready = undefined;
};

DemoTextModel.prototype.getText = function() {
    if(this.ready === undefined) {
        this.ready = this.demoTextService.get();
    }
    return this.ready;
}

module.exports = DemoTextModel;

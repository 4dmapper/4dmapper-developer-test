var Promise = require('bluebird');

function DemoTextService(app) {
    this.app = app;
}

DemoTextService.prototype.get = function(){
    return Promise.delay(3000).throw('');
}

module.exports = DemoTextService;

var knockout = require('knockout-es5');
var _ = require('lodash');
var fs = require('fs');
var loadView = require('../core/loadView');

function DemoTextViewModel(app, demoTextModel){
    var self = this;
    this.app = app;
    this.demoTextModel = demoTextModel;

    this.demo_text = "";

    knockout.track(this, ['demo_text']);
}

DemoTextViewModel.prototype.load = function(){
    var taskListViewHtml = fs.readFileSync(__dirname + '/DemoTextView.html', 'utf8');
    return loadView(taskListViewHtml, '#demo-text', this);
}

module.exports = DemoTextViewModel;

"use strict";

var DemoTextViewModel = require('./DemoText/DemoTextViewModel');
var DemoTextService = require('./DemoText/DemoTextService');
var DemoTextModel = require('./DemoText/DemoTextModel');

function Application() {
    this.demoTextService = new DemoTextService(this);
    this.demoTextModel = new DemoTextModel(this, this.demoTextService);
    this.demoTextViewModel = new DemoTextViewModel(this, this.demoTextModel);
}

Application.prototype.load = function(){
    this.demoTextViewNodes = this.demoTextViewModel.load();
}

var app = new Application();
app.load();

var request = require('request-promise');

var url = require('url');
var _ = require('lodash');

var href = document.location.href;
var urlObj = url.parse(href);

function TaskListService(app) {
    this.app = app;
}

TaskListService.prototype.get = function(){
    var uri = _.clone(urlObj);
    uri.path = '/tasks';
    return request({
        uri:  uri,
        json: true
    });
}

module.exports = TaskListService;

"use strict";

var Promise = require('bluebird');

function TaskListModel(app, taskListService) {
    this.app = app;
    this.taskListService = taskListService;

    this.tasksReady = undefined;
};

TaskListModel.prototype.getTasks = function() {
    if(this.tasksReady === undefined) {
        this.tasksReady = this.taskListService.get();
    }
    return this.tasksReady;
}

module.exports = TaskListModel;

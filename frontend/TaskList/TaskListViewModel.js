var knockout = require('knockout-es5');
var _ = require('lodash');
var fs = require('fs');
var loadView = require('../core/loadView');

function TaskListViewModel(app, taskListModel){
    var self = this;
    this.app = app;
    this.taskListModel = taskListModel;

    this.loading = 0;
    this.tasks = [];
    this.sTasks = [];
    this.rooms = [];

    knockout.track(this,[
        'loading',
        'tasks',
        'rooms',
        'sTasks',
    ]);

    knockout.getObservable(this, 'tasks').subscribe(function(){
        ++self.loading;
        function pad(num, size) {
            var s = num+"";
            while (s.length < size) s = "0" + s;
            return s;
        }
        function desc(item){
            return item.room + "/" + item.place + "/" + item.task + "/" + pad(item.part,10);
        }

        var items = _.clone(this.tasks);

        var length = items.length;
        if(length > 2){
            for (var i = (length - 1); i >= 0; i--) {
                for (var j = (length - i - 1); j > 0; j--) {
                    if (desc(items[j]) < desc(items[j - 1])) {
                        var tmp = items[j];
                        items[j] = items[j - 1];
                        items[j - 1] = tmp;
                    }
                }
            }
        }
        this.sTasks = items;
    }, this);

    knockout.getObservable(this, 'tasks').subscribe(function(){
        ++self.loading;
        var rooms = [];
        _.forEach(this.tasks, function(t){
            rooms = _.uniq(_.concat(self.rooms,t.room));
        });
        self.rooms = rooms;
        --self.loading;
    }, this);

    ++self.loading;
    taskListModel.getTasks().then(function(tasks){
        self.tasks = [];
        for(var i=0; i< tasks.length; ++i) {
            self.tasks.push(tasks[i]);
        }
        --self.loading;
    });
}

TaskListViewModel.prototype.load = function(){
    var taskListViewHtml = fs.readFileSync(__dirname + '/TaskListView.html', 'utf8');
    return loadView(taskListViewHtml, '#task-list', this);
}

module.exports = TaskListViewModel;

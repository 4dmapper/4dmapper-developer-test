'use strict';

/*global require*/
var knockout = require('knockout-es5');
var $ = require("jquery");

var createFragmentFromTemplate = require('./createFragmentFromTemplate');

// load htmlString into container and apply knockout bindings to viewModel
var loadView = function(htmlString, containerSelector, viewModel) {
    var container = $(containerSelector)[0];

    var fragment = createFragmentFromTemplate(htmlString);

    var nodes = [];
    var i;
    for (i = 0; i < fragment.childNodes.length; ++i) {
        nodes.push(fragment.childNodes[i]);
    }

    container.appendChild(fragment);

    for (i = 0; i < nodes.length; ++i) {
        var node = nodes[i];
        if (node.nodeType === Node.ELEMENT_NODE || node.nodeType === Node.COMMENT_NODE) {
            knockout.applyBindings(viewModel, node);
        }
    }

    return nodes;
};

module.exports = loadView;

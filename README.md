# 4DMapper Developer Test Scenario

Thanks for your interest in 4Dmapper.

In order to evaluate potential candidates in a development role, we have prepared a development task in the form of a git repository https://bitbucket.org/4dmapper/4dmapper-developer-test

* Follow the startup instructions
* Fork and clone the git repository
* Undertake the development tasks and write in the development log
* Share your submission back via git

The scope of this test could be quite long and we appreciate any time that you are able to invest in looking into it.
Thank you for participating in the 4Dmapper development test.

------------------------------------

Startup Instructions
====================

## Obtain access
You will need a user account on https://bitbucket.org.
Access to this repository is public but your submission should be via a private fork.

## Fork the repository
Use https://bitbucket.org/4dmapper/4dmapper-developer-test/fork to create a private fork on your own bitbucket user account.
Please fork as "Access level **private** This is a private repository"

## Make local clone of your fork
Follow bitbucket instructions to fork your repository to your local machine.
eg:

    git clone git clone git@bitbucket.org:YourUser/4dmapper-developer-test.git
    cd 4dmapper-developer-test

## Install dependencies
For this test and for development at 4dmapper, running on Linux (Ubuntu or Debian) is recommended.
The included ./install-deps.sh installs node and NPM for Ubuntu and/or Debian.

    ./install-deps.sh

Alternatively you may have a different platform or preferred method of installing node & NPM.

Finally, run npm install

    npm install


## Setup Git Config

    git config user.name "First Last"
    git config user.email "first.last@host.com"

## Setup Git branch
Use branches prefixed by your email address

    git checkout -b $(git config --get user.email)/develop

## Start Server

    npm start
    #browse to http://localhost:8080/

Development Evaluatation and Test Tasks
=======================================

# Communication

## Git
### Create your submission as a series of discrete, linear commits
Use git rebase to reformat your commits into a clean linear sequence of commits, in order to clean up any work-in-progress commits and generally present a clear set of commits for the work presented.

### Use well formatted commit messages to describe your work
See http://chris.beams.io/posts/git-commit/

### Branches
Create multiple feature branches, rebased ahead of each other, for disctinct aspects of the problem.

## Text development log
Add to development.md to create bug/feature items as if they were Jira bug/feature reports.

* Write about any development or technical issues present in the existing codebase or that you encounter along the way.
* Write about your solutions against the issues as you proceed.
* Write about some further possible features or development tasks for this codebase.

# Frontend development

See http://localhost:8080/frontend-dev-test.html

## Context

We have a large number of structured items to display and manipulate - in this scenario it is a todo list for various rooms and places to be cleaned.

## Goals

The main goal is to improve the display of the task list into a clean, fast, responsive design.

* The user wants to be able to operate the app quickly.
* Marketing wants the app to look attractive.
* The user wants to be able to see the tasks by assignee and by room.
* The user wants to be able to use different screen sizes from mobile to desktop.

# Backend development

## Context

As a simple frontend-backend test we setup a simple example:

We want to implement a simple shared blackboard system where any user viewing the page can write in a text area and have that saved and shared with other viewers of the page.

## Goals

The development task constraints are that:

* The implementation shall extend the existing node.js backend.
* The backend must expose an HTTP API.
* The backend must use a Postgres database and node.js Postgres client to store and access the data.
* Your code should work as-submitted to connect to and use your Postgres instance.

Tip:  Create a free Postgres database instance on www.elephantsql.com

The main goals are that:

* The user wants to quickly glance at the page and see the latest text content
* The user wants to be able to add to or modify the text content and know that other users will be able to see it.

# Full Stack development

Extend your API and database in order to extend the frontend problem:

* Mark tasks as done
* Create, edit and delete tasks

Submission Instructions
====================

## Create a branch for your submission

    git checkout -b $(git config --get user.email)/submission

## Check your git history
Check that your git history is clean and suitable for review.
Use git rebase to fix it if necessary.

## Push your branch to your private fork

## Add reviewer(s) with read permissions
Use https://bitbucket.org to add 4dmapper reviewer(s) with read permissions on your private fork:

* User `p_r_thompson` (https://bitbucket.org/p_r_thompson/)

## Email us to notify of your completion.
Email paul.thompson@4dmapper.com with your bitbucket username and the SSH URL of your private fork.

eg:

    git@bitbucket.org:FirstLast/4dmapper-developer-test.git

Thank You
=========
Thank you for participating in the 4Dmapper development test.
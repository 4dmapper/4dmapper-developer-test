#!/bin/sh

# When merging, use an explicit merge commit, even if it's simply fast-forward
git config merge.ff false

# In case there's edits on this branch when pulling in from upstream, use rebase instead of merge to update
git config pull.rebase true

# Use filters before merging to prevent whitespace conflicts
git config merge.renormalize true


# Define filter spaces2 and spaces4 for use in .gitattributes with aim of having 2 or 4 spaces in working checkouts and repo.
git config filter.spaces4.smudge 'expand --initial --tabs=4 | sed "s/[[:space:]]\+$//" | sed -e "\$a\\"'
git config filter.spaces4.clean 'expand --initial --tabs=4 | sed "s/[[:space:]]\+$//" | sed -e "\$a\\"'

git config filter.spaces2.smudge 'expand --initial --tabs=2 | sed "s/[[:space:]]\+$//" | sed -e "\$a\\"'
git config filter.spaces2.clean 'expand --initial --tabs=2 | sed "s/[[:space:]]\+$//" | sed -e "\$a\\"'

'use strict';

var browserify = require('browserify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
//var uglify = require('gulp-uglify');
//var sourcemaps = require('gulp-sourcemaps');
//var reactify = require('reactify');

gulp.task('build-backend-dev-app', function () {
  var b = browserify({
    entries: './frontend/frontend-dev-main.js'
  }).transform('brfs');

  return b.bundle()
    .pipe(source('frontend-dev-app.js'))
    .pipe(buffer())
    .pipe(gulp.dest('./public/build/'));
});

gulp.task('build-frontend-dev-app', function () {
  var b = browserify({
    entries: './frontend/backend-dev-main.js'
  }).transform('brfs');

  return b.bundle()
    .pipe(source('backend-dev-app.js'))
    .pipe(buffer())
    .pipe(gulp.dest('./public/build/'));
});

gulp.task('build', ['build-frontend-dev-app', 'build-backend-dev-app'])

gulp.task('default', ['build']);
